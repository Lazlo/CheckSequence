#include "CheckSequence.h"

#define NUMBERS_MISSING(from, to)	(to - 1 - from)
#define NUMBERS_LATE(from, to)		(to - from)

static inline void CheckSequence_ResetCounters(struct CheckSequenceStateStruct *s) {
	s->counter.total = 0;
	s->counter.missing = 0;
	s->counter.late = 0;
	s->counter.doubles = 0;
	s->counter.overflow = 0;
}

static inline void CheckSequence_Update(struct CheckSequenceStateStruct *s, const csnum_t number)
{
	s->last = s->current;
	s->current = number;
}

void CheckSequence_Create(struct CheckSequenceStateStruct *s)
{
	s->first = 0;
	s->last = 0;
	s->current = 0;
	CheckSequence_ResetCounters(s);
}

void CheckSequence_Destroy(struct CheckSequenceStateStruct *s)
{
}

int8_t CheckSequence_Add(struct CheckSequenceStateStruct *s, const csnum_t number)
{
	int8_t rc = 127;
	const csnum_t max = ~(csnum_t)0;
	csnum_t current = s->current;
	csnum_t next = current < max ? current + 1 : 0;

	if (s->counter.total == 0)
		s->first = number;

	s->counter.total++;

	/* number compared to next */

	if (number >= next) {
		if (current == max)
			s->counter.overflow++;
		CheckSequence_Update(s, number);
		/* is the next number we are looking for */
		if (number == next) {
			rc = CS_ADD_RC_NEXT;
		}
		/* looks like there was a gap - we missed some numbers */
		else {
			s->counter.missing += NUMBERS_MISSING(current, number);
			rc = CS_ADD_RC_MISSING;
		}
	}

	/* number compared to current */

	else {
		/* some smaller number - late delivery */
		if (number < current) {
			s->counter.late++;
			s->counter.missing--;
			rc = CS_ADD_RC_LATE;
		}
		/* double - we had that one already */
		else {
			s->counter.doubles++;
			rc = CS_ADD_RC_DOUBLE;
		}
	}
	return rc;
}

csnum_t CheckSequence_GetFirst(const struct CheckSequenceStateStruct *s)
{
	return s->first;
}

csnum_t CheckSequence_GetLast(const struct CheckSequenceStateStruct *s)
{
	return s->last;
}

csnum_t CheckSequence_GetCurrent(const struct CheckSequenceStateStruct *s)
{
	return s->current;
}

csnum_t CheckSequence_GetMissing(const struct CheckSequenceStateStruct *s)
{
	csnum_t missing = NUMBERS_MISSING(s->last, s->current);
	return missing;
}

csnum_t CheckSequence_GetLateDelay(const struct CheckSequenceStateStruct *s, const csnum_t number)
{
	return NUMBERS_LATE(number, s->current);
}
