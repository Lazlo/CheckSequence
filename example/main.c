#include "CheckSequence.h"
#include <stdio.h>
#include <stdint.h>
#include <signal.h>

static uint8_t run = 1;

static void sighandler(int signo)
{
	run = 0;
}

static uint64_t test = 1;

static uint64_t SomeFunctionToReadANumber(void)
{
	if (test >= 100 && test % 100 == 0)
		test += 25;
	else if (test % 100 == 25) {
		test++;
		return test - 25;
	} else
		test++;
	return test;
}

static struct CheckSequenceStateStruct s;

int main(int argc, char *argv[])
{
	uint64_t start = 0;
	uint64_t n;
	int8_t rc;

	struct sigaction act;
	act.sa_handler = sighandler;
	sigaction(SIGINT, &act, NULL);

	CheckSequence_Create(&s, start);
	while (run && test <= 1000) {
		n = SomeFunctionToReadANumber();
		rc = CheckSequence_Add(&s, n);
		if (rc == 0) {
			continue;
		}

		printf("Last %ld. Current %ld. ",
			CheckSequence_GetLast(&s),
			CheckSequence_GetCurrent(&s));
		switch (rc) {
		case CS_ADD_RC_LATE:
			printf("Late %ld (Delayed by %ld)\n", n, CheckSequence_GetLateDelay(&s, n));
			break;
		case CS_ADD_RC_DOUBLE:
			printf("Double number %ld\n", n);
			break;
		case CS_ADD_RC_MISSING:
			printf("Missing %ld\n",
				CheckSequence_GetMissing(&s));
			break;
		default:
			break;
		}
	}
	uint8_t i;
	for (i = 0; i < 72; i++)
		printf("=");
	printf("\n");
	printf("First: %ld\n", CheckSequence_GetFirst(&s));
	printf("Last: %ld\n", CheckSequence_GetCurrent(&s));
	printf("Total: %ld\n", s.counter.total);
	printf("Missing: %ld\n", s.counter.missing);
	printf("Late: %ld\n", s.counter.late);
	printf("Doubles: %ld\n", s.counter.doubles);
	printf("Overflow: %ld\n", s.counter.overflow);
	return 0;
}
