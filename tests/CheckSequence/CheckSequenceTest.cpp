extern "C"
{
#include "CheckSequence.h"
}

#include "CppUTest/TestHarness.h"

static struct CheckSequenceStateStruct s;

TEST_GROUP(CheckSequence)
{
    void setup()
    {
      CheckSequence_Create(&s);
    }

    void teardown()
    {
       CheckSequence_Destroy(&s);
    }
};

TEST(CheckSequence, GetLast_isZeroAfterCreate)
{
	CHECK_EQUAL(0, CheckSequence_GetLast(&s));
}

TEST(CheckSequence, GetCurrent_isZeroAfterCreate)
{
	CHECK_EQUAL(0, CheckSequence_GetCurrent(&s));
}

TEST(CheckSequence, Create_SetsTotalCounterToZero)
{
	CHECK_EQUAL(0, s.counter.total);
}

TEST(CheckSequence, Create_SetsMissingCounterToZero)
{
	CHECK_EQUAL(0, s.counter.missing);
}

TEST(CheckSequence, Create_SetsLateCounterToZero)
{
	CHECK_EQUAL(0, s.counter.late);
}

TEST(CheckSequence, Create_SetsDoublesCounterToZero)
{
	CHECK_EQUAL(0, s.counter.doubles);
}

TEST(CheckSequence, Create_SetsOverflowCounterToZero)
{
	CHECK_EQUAL(0, s.counter.overflow);
}

TEST(CheckSequence, Add_willUpdateCurrentIfGreaterThanLast)
{
	CheckSequence_Add(&s, 32);
	CHECK_EQUAL(32, CheckSequence_GetCurrent(&s));
}

TEST(CheckSequence, Add_willUpdateLast)
{
	CheckSequence_Add(&s, 1);
	CheckSequence_Add(&s, 2);
	CHECK_EQUAL(1, CheckSequence_GetLast(&s));
}

TEST(CheckSequence, Add_willNotUpdateCurrentIfEqualOrSmallerThanLast)
{
	CheckSequence_Add(&s, 7);
	CheckSequence_Add(&s, 6);
	CHECK_EQUAL(7, CheckSequence_GetCurrent(&s));
}

TEST(CheckSequence, Add_returnsZeroWhenNumberIsOneLargerThanCurrent)
{
	CHECK_EQUAL(0, CheckSequence_Add(&s, 1));
}

TEST(CheckSequence, Add_returnsTwoIfNumberIsLargerCurrentPlusOne)
{
	CheckSequence_Add(&s, 1);
	CHECK_EQUAL(2, CheckSequence_Add(&s, 5));
}

TEST(CheckSequence, Add_returnsOneIfNumberIsEqualCurrenNumber)
{
	CHECK_EQUAL(1, CheckSequence_Add(&s, 0));
}

TEST(CheckSequence, Add_returnsNegativeOneIfSmallerThanCurrent)
{
	uint8_t i;
	for (i = 1; i <  10; i++)
		CheckSequence_Add(&s, i);
	CHECK_EQUAL(-1, CheckSequence_Add(&s, 5));
}

TEST(CheckSequence, Add_incrementsTotalCounter)
{
	uint8_t i;
	for (i = 1; i <= 30; i++)
		CheckSequence_Add(&s, i);
	CHECK_EQUAL(30, s.counter.total);
}

TEST(CheckSequence, Add_incrementsMissingCounterIfNumberIsGreaterCurrentPlusOne)
{
	CheckSequence_Add(&s, 1);
	CheckSequence_Add(&s, 30);
	CHECK_EQUAL(28, s.counter.missing);
}

TEST(CheckSequence, Add_incrementsLateCounterIfNumberIsSmallerCurrent)
{
	CheckSequence_Add(&s, 1);
	CheckSequence_Add(&s, 2);
	CheckSequence_Add(&s, 1);
	CHECK_EQUAL(1, s.counter.late);
}

/* When we received a late number - we assume we had a gap before - hence we
 * marked it as missing */
TEST(CheckSequence, Add_decrementsMissingIfNumberIsSmallerCurrent)
{
	CheckSequence_Add(&s, 1);
	CheckSequence_Add(&s, 8);
	CheckSequence_Add(&s, 9);
	CheckSequence_Add(&s, 10);
	CheckSequence_Add(&s, 2);
	CheckSequence_Add(&s, 3);
	CheckSequence_Add(&s, 4);
	CHECK_EQUAL(3, s.counter.missing);
}

TEST(CheckSequence, Add_incrementsDoublesCounterIfNumerIsEqualCurrent)
{
	CheckSequence_Add(&s, 0);
	CheckSequence_Add(&s, 0);
	CheckSequence_Add(&s, 0);
	CHECK_EQUAL(3, s.counter.doubles);
}

TEST(CheckSequence, Add_incrementsOverflowCounterIfCurrentIsMaxValueAndNumberIsZero)
{
	csnum_t max = ~(csnum_t)0;
	CheckSequence_Add(&s, max);
	CheckSequence_Add(&s, 0);
	CHECK_EQUAL(1, s.counter.overflow);
}

IGNORE_TEST(CheckSequence, Add_onFirstNumberWillNotIncrementMissing)
{
	CheckSequence_Add(&s, 40);
	CHECK_EQUAL(0, s.counter.missing);
}

TEST(CheckSequence, GetMissing)
{
	CheckSequence_Add(&s, 1);
	CheckSequence_Add(&s, 10);
	CHECK_EQUAL(8, CheckSequence_GetMissing(&s));
}

TEST(CheckSequence, GetLateDelay)
{
	uint8_t i;
	for (i = 2; i <= 10; i++)
		CheckSequence_Add(&s, i);
	CheckSequence_Add(&s, 1);
	CHECK_EQUAL(9, CheckSequence_GetLateDelay(&s, 1));
}

TEST(CheckSequence, GetFirst)
{
	uint8_t i;
	for (i = 99; i <= 150; i++)
		CheckSequence_Add(&s, i);
	CHECK_EQUAL(99, CheckSequence_GetFirst(&s));
}
