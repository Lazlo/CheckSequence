#ifndef D_CheckSequenceBuildTime_H
#define D_CheckSequenceBuildTime_H

///////////////////////////////////////////////////////////////////////////////
//
//  CheckSequenceBuildTime is responsible for recording and reporting when
//  this project library was built
//
///////////////////////////////////////////////////////////////////////////////

class CheckSequenceBuildTime
  {
  public:
    explicit CheckSequenceBuildTime();
    virtual ~CheckSequenceBuildTime();
    
    const char* GetDateTime();

  private:
      
    const char* dateTime;

    CheckSequenceBuildTime(const CheckSequenceBuildTime&);
    CheckSequenceBuildTime& operator=(const CheckSequenceBuildTime&);

  };

#endif  // D_CheckSequenceBuildTime_H
