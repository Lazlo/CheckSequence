#include "CheckSequenceBuildTime.h"

CheckSequenceBuildTime::CheckSequenceBuildTime()
: dateTime(__DATE__ " " __TIME__)
{
}

CheckSequenceBuildTime::~CheckSequenceBuildTime()
{
}

const char* CheckSequenceBuildTime::GetDateTime()
{
    return dateTime;
}

