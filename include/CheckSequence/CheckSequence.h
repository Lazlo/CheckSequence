#ifndef D_CheckSequence_H
#define D_CheckSequence_H

/**********************************************************
 *
 * CheckSequence is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

typedef uint64_t csnum_t;

struct CheckSequenceCounterStruct {
	csnum_t total;
	csnum_t missing;
	csnum_t late;
	csnum_t doubles;
	csnum_t overflow;
};

struct CheckSequenceStateStruct {
	csnum_t first;
	csnum_t last;
	csnum_t current;
	struct CheckSequenceCounterStruct counter;
};

enum {
	CS_ADD_RC_LATE = -1,
	CS_ADD_RC_NEXT = 0,
	CS_ADD_RC_DOUBLE = 1,
	CS_ADD_RC_MISSING = 2
};

void CheckSequence_Create(struct CheckSequenceStateStruct *s);
void CheckSequence_Destroy(struct CheckSequenceStateStruct *s);

int8_t CheckSequence_Add(struct CheckSequenceStateStruct *s, const csnum_t number);

csnum_t CheckSequence_GetFirst(const struct CheckSequenceStateStruct *s);
csnum_t CheckSequence_GetLast(const struct CheckSequenceStateStruct *s);
csnum_t CheckSequence_GetCurrent(const struct CheckSequenceStateStruct *s);
csnum_t CheckSequence_GetMissing(const struct CheckSequenceStateStruct *s);
csnum_t CheckSequence_GetLateDelay(const struct CheckSequenceStateStruct *s, const csnum_t number);

#endif  /* D_FakeCheckSequence_H */
