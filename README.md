# CheckSequence

Is a library that allows checking a sequence of numbers (written in C).

It was written [test-first](http://www.extremeprogramming.org/rules/testfirst.html) using [CppUTest](https://cpputest.github.io/).

[![Build Status](https://travis-ci.org/lazlo/CheckSequence.svg?branch=master)](https://travis-ci.org/lazlo/CheckSequence)

## Features

It detects ...
 * numbers missing
 * numbers out of order
 * numbers the occur more than once
 * overflows of the numbers counted

It features
 * five counters
   * total
   * missing
   * late
   * doubles
   * overflows
 * save first number
 * calculate numbers missing
 * calculate delay of late number

## Limitations

Limitations:
 * only works on positive numbers
 * build around uint64\_t

## Usage

Its usage is straight forward.

 * allocate memory for struct CheckSequenceStateStruct
 * call CheckSequence\_Create() passing a pointer to the struct
 * call CheckSequence\_Add() passing a pointer to the struct and a number, optionally check its return code
 * inspect counter values in state struct

For an example see the example/main.c file.
