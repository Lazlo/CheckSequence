#include "CppUTest/TestHarness.h"
#include "CheckSequenceBuildTime.h"

TEST_GROUP(CheckSequenceBuildTime)
{
  CheckSequenceBuildTime* projectBuildTime;

  void setup()
  {
    projectBuildTime = new CheckSequenceBuildTime();
  }
  void teardown()
  {
    delete projectBuildTime;
  }
};

TEST(CheckSequenceBuildTime, Create)
{
  CHECK(0 != projectBuildTime->GetDateTime());
}

